from rest_framework.test import APITestCase
from django.core.urlresolvers import reverse
from .models import Category, Subcategory

class CategoryTests(APITestCase):
    fixtures = ['closet/fixtures/category.json']

    def test_list(self):
        url = reverse('category-list')
        response = self.client.get(url)
        self.assertEqual(len(response.data), Category.objects.count())


class SubcategoryTests(APITestCase):
    fixtures = ['closet/fixtures/category.json', 'closet/fixtures/subcategory.json']

    def test_list(self):
        url = reverse('subcategory-list')
        response = self.client.get(url)
        self.assertEqual(len(response.data), Subcategory.objects.count())
