from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from closet.models import Category, Subcategory
from closet.serializers import SubcategorySerializer, CategorySerializer
from maps.serializers import MarkerSerializer
from maps.models import Marker


class SubcategoryViewSet(viewsets.ModelViewSet):
    queryset = Subcategory.objects.all()
    serializer_class = SubcategorySerializer

    @detail_route()
    def markers(self, request, pk=None):
        """ List all the markers for a given subcategory. Its route is
        subcategories/{pk}/markers. """
        queryset = Marker.objects.filter(subcategories__id=pk)
        serializer = MarkerSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.prefetch_related('subcategory_set').all()
    serializer_class = CategorySerializer

    @detail_route()
    def markers(self, request, pk=None):
        """ List all the markers for a given category. Its route is
        catagories/{pk}/markers. """
        category = Category.objects.get(pk=pk)
        queryset = Marker.objects.filter(
            subcategories__in=category.subcategory_set.all())
        serializer = MarkerSerializer(queryset, many=True)
        return Response(serializer.data)
