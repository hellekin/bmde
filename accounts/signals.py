from registration.signals import user_activated
from django.dispatch import receiver
from django.contrib.auth.models import Group


@receiver(user_activated)
def user_activated_can_modify_marker(sender, **kwargs):
    """
    Give right to the new user_activated to modify markers (user is in the
    staff and in the group marker-master)
    """
    user = kwargs['user']
    basic_user_group = Group.objects.get(name='marker-master')
    user.groups.add(basic_user_group)
    user.is_staff = True
    user.save()
