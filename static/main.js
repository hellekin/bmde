/* jslint vars: true */
/*jslint indent: 3 */
/* global require, document */
'use strict';

require.config({
    paths: {
        'jquery': 'lib/jquery/jquery',
        'leaflet': 'lib/leaflet/leaflet-src',
        'leaflet.markercluster': 'lib/leaflet.markercluster/leaflet.markercluster-src',
        'Leaflet.FeatureGroup.SubGroup': 'lib/Leaflet.FeatureGroup.SubGroup/dist/leaflet.featuregroup.subgroup-src',
        'snapjs': 'lib/Snap.js/snap',
        'mustache': 'lib/mustache.js/mustache',
        'foundation': 'lib/foundation/foundation',
        'qtip2': 'lib/qtip2/jquery.qtip',
    },
    shim: {
        'foundation': { deps: ['jquery'] },
        'snapjs': {exports: 'Snap'},
        'qtip2': {deps: ['jquery']},
        'leaflet.markercluster': {deps: ['leaflet']},
        'Leaflet.FeatureGroup.SubGroup': {deps: ['leaflet']},
        'minimarker': {deps: ['leaflet']},
    }
});

require(['leaflet', 'markers', 'subcategories', 'map', 'cluster', 'snapjs',
    'jquery', 'jquery', 'foundation', 'qtip2', 'minimarker',
    'leaflet.markercluster', 'Leaflet.FeatureGroup.SubGroup'],
    function(L, markers, subcategories, map, cluster, Snap, $, jQuery,
        foundation) {

        map.bmdeInit();
        cluster.init();

        $(document).ready(function () {
            var tooltip = $('a[title]').qtip({
                position:{
                    my: 'top center',
                    at: 'bottom center',
                    corner:{target:'leftMiddle',tooltip:'rightMiddle'}, //instead of corner:{target:'rightMiddle',tooltip:'leftMiddle'},
                    adjust:{screen:true, resize:true}
                },
                show: 'click',
                hide: {
                    event: 'unfocus'
                },
            });
        });

        var width = document.getElementById('snapsDrawer').offsetWidth;
        var snapper = new Snap({
            element: document.getElementById('content'),
            disable: 'right',
            tapToClose: false,
            maxPosition: width,
            minPosition: -width,
        });
        snapper.open("left");
        $("#menuButton").click(function(){
            if( snapper.state().state=="left" ) {
                snapper.close();
            } else {
                snapper.open('left');
            }
        });
        $("#menuDrawerTop").click(function(){
            if( snapper.state().state=="left" ) {
                snapper.close();
            } else {
                snapper.open('left');
            }
        });
        $(document).foundation({
            accordion: {
                // specify the class used for accordion panels
                content_class: 'content',
                // specify the class used for active (or open) accordion panels
                active_class: 'active',
                // allow multiple accordion panels to be active at the same time
                multi_expand: true,
                toggleable: true
            }
        });

        subcategories.watchSubCatSelectionMenu();
    }
);
