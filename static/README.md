Install the js and css dependencies
===================================

Grunt (and bower) will install all the js & css dependencies into the
`./lib` folder.

To install :
------------

```bash
npm install
grunt
```

For dev (autoupdate):
---------------------

```bash
grunt watch
```
