/* global define */
'use strict';

define(['leaflet', 'map', 'jquery', 'mustache'],
    function(L, map, $, Mustache) {
        var json_markers = [];
        var markers_leaflet_group = new L.FeatureGroup();

        function getPopupContent(marker) {
            /*
             * Get the popup content for a marker (see API)
             * @param {object} marker The marker
             */

            var template = $('#popup-template').html();
            Mustache.tags = ['[[', ']]'];
            Mustache.parse(template);   // optional, speeds up future uses
            return Mustache.render(template, {marker: marker});
        }

        function getMarker(data) {
            /**
             * Get a new marker from data describing a point on the map (see api)
             * @param {data} data Data describing a point on the map
             */
            var icon = L.MakiMarkers.icon({color: data.color, size: "l"});
            var marker = L.marker([data.lat, data.lon], {icon: icon})
                .bindPopup(getPopupContent(data))
                .on("click", function(e) {
                    map.panTo(e.latlng);
                });
            return marker;
        }

        return {
            getMarker: getMarker,
        };
    }
);
