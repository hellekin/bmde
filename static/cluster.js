/* global define */
'use strict';

define(['map', 'leaflet', 'leaflet.markercluster', 'Leaflet.FeatureGroup.SubGroup'],
    function(map, L) {
        var clusterGroup = L.markerClusterGroup({
                disableClusteringAtZoom: 13,
                spiderfyOnMaxZoom: false,
            });

        function init() {
            /**
             * Initialization
             */
            clusterGroup.addTo(map);
        }

        function addFeatureGroup(g) {
            /**
             * Adding a feature group to the cluster (and display its elements
             * on the map)
             * @param {FeatureGroup} g The featureGroup to display
             */
            g.setParentGroup(clusterGroup);
            map.addLayer(g);
            //g.addTo(map);
        }

        function rmFeatureGroup(g) {
            /**
             * Removing a feature group from the cluster (and hide its elements
             * from the map)
             * @param {FeatureGroup} g The featureGroup to hide
             */
            map.removeLayer(g);
        }

        return {
            init: init,
            addFeatureGroup: addFeatureGroup,
            rmFeatureGroup: rmFeatureGroup,
        };
    }
);
