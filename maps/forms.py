from closet.models import Category, Subcategory
from django import forms
from django.core.exceptions import ValidationError

def validate_subcat(input):
    sub = input.split(",")
    # if len(sub) < 1:
    #     raise ValidationError(
    #        "Il faut au moins sélectionner une sous-catégorie")
    for cat in sub:
        if not cat.isnumeric():
            ValidationError("Catégorie invalide")

# TODELETE
class MarkerForm(forms.Form):
    name = forms.CharField(required=True)
    web = forms.URLField(required=False)
    phone = forms.CharField(required=False)
    address = forms.CharField(required=False)
    lat = forms.FloatField(required=True)
    lon = forms.FloatField(required=True)
    description = forms.CharField(required=False)
    subcat = forms.CharField(validators=[validate_subcat])


class CSVImportChooseCatForm(forms.Form):
    """ Form to choose the category before uploading a csv file """
    category = forms.ModelChoiceField(
        queryset=Category.objects.all(), empty_label=None)


class CSVImportForCatForm(forms.Form):
    """ Form to import csv file into a subcategory. A category is given via
    the url """
    sub_category = forms.ModelMultipleChoiceField(queryset=Subcategory.objects.all())
    csv_file = forms.FileField()
    #  TODELETE csv_old = forms.CharField(required=True, widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        cat_id = kwargs.pop('cat_id')
        super(CSVImportForCatForm, self).__init__(*args, **kwargs)
        self.fields['sub_category'].queryset = Subcategory.objects.filter(
            category__id=cat_id)
