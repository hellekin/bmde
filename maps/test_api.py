from rest_framework.test import APITestCase
from django.core.urlresolvers import reverse
from .models import Marker
from django.test import Client
import json


class MarkerTests(APITestCase):
    fixtures = ['fixtures/mini_db_for_test.json']

    def test_list(self):
        url = reverse('marker-list')
        response = self.client.get(url)
        self.assertEqual(len(response.data), Marker.objects.count())

    def test_data(self):
        marker = Marker.objects.first()
        c = Client()
        url = reverse('marker-detail', kwargs={'pk': marker.id})
        response = c.get(url)
        self.assertEqual(response.status_code,200)
        marker_json = json.loads(response.content.decode("utf-8"))
        self.assertEqual(marker.name, marker_json['name'])
