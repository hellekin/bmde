from django.http import HttpResponse, HttpResponseBadRequest
from rest_framework import viewsets
from maps.serializers import MarkerSerializer
from django.views.decorators.csrf import csrf_exempt
from django.contrib.gis.geos import Point
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from maps.models import Marker
from closet.models import Subcategory, Category
import csv
from django.db import transaction
from maps.forms import CSVImportChooseCatForm, CSVImportForCatForm, MarkerForm
from io import TextIOWrapper
from rest_framework.views import APIView
from rest_framework.response import Response
import reversion


class MarkerViewSet(viewsets.ModelViewSet):
    queryset = Marker.objects \
        .prefetch_related('subcategories').filter(public=True)
    serializer_class = MarkerSerializer


class MarkerHistoryView(APIView):
    """
    View to list the history for a given marker.
    """
    def get(self, request, marker_id):
        """
        For a given marker list its history
        """
        marker = Marker.objects.get(pk=marker_id)
        return Response(marker.get_history_for_json())


@login_required
def csv_import_choose_cat(request):
    """
    View with a form to choose a category for importing a csv file
    """
    if request.method == 'POST':
        form = CSVImportChooseCatForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            return redirect('csv_import_for_cat', cat_id=data['category'].id)
        else:
            return render(request, 'maps/csv_import_form.html', {'form': form})
    else:
        form = CSVImportChooseCatForm()
        return render(request, 'maps/csv_import_form.html', {'form': form})

@login_required
def csv_import_for_cat(request, cat_id):
    """
    View whith a form to choose a subcategory and upload a csv file. The
    csv file contains new markers that will be added to the subcategory.

    :param cat_id: The id of the subcategories (chosen by the user at the
    previous step).
    """
    if request.method == 'POST':
        form = CSVImportForCatForm(request.POST, request.FILES, cat_id=cat_id)
        if form.is_valid():
            subcat_id = form.cleaned_data['sub_category']
            csvfile = None
            encodings = ['utf-8', 'windows-1250', 'windows-1252']
            for e in encodings:
                try:
                    request.FILES['csv_file'].file.seek(0)
                    request.FILES['csv_file'].file.read().decode(e)
                    request.FILES['csv_file'].file.seek(0)
                    csvfile = TextIOWrapper(
                        request.FILES['csv_file'].file, encoding=e)
                except UnicodeDecodeError:
                    # si pbm d'encodage la ligne
                    # request.FILES['csv_file'].file.read().decode(e)
                    # va lancer une exception -> on cherche un nouvel encodage
                    # print('got unicode error with %s , trying different encoding' % e)
                    pass
                else:
                    # l'encodage a ete trouve -> csvfile contient une bonne donnée
                    # print('opening the file with encoding:  %s ' % e)
                    break
            if csvfile:
                return import_csv(request, csvfile, subcat_id)
            else:
                return render(
                    request, 'www/basic_message.html', {
                        'message': 'Encodage non trouvé',
                        'message_type': 'alert',
                    })
        else:
            return render(request, 'maps/csv_import_form.html', {'form': form})
    else:
        form = CSVImportForCatForm(cat_id=cat_id)
        return render(request, 'maps/csv_import_form.html', {'form': form})


@transaction.non_atomic_requests  # either all the elements are imported in db
# either none of the elements are imported in db
def import_csv(request, csvfile, subcat_id):
    """
    Read the CSV file and load it in database
    """
    required_fields = ['name', 'lat', 'lon', 'address']
    optional_fields = ['comment', 'web', 'email', 'phone']

    dialect = csv.Sniffer().sniff(csvfile.read())
    csvfile.seek(0)
    reader = csv.reader(csvfile, dialect)

    lines = list(reader)
    header = lines.pop(0)

    points, error_message = get_and_check_csv_data(
        required_fields, optional_fields, header, lines)

    if points:
        for point in points:
            pos = Point(float(point['lon']), float(point['lat']))
            with reversion.create_revision():
                m = Marker.objects.create(
                    name=point['name'],
                    address=point['address'],
                    position=pos)

                for fo in optional_fields:
                    if fo in point:
                        setattr(m, fo, point[fo])

                m.subcategories.add(Subcategory.objects.get(pk=subcat_id))
                m.save()
                reversion.set_user(request.user)
                reversion.set_comment("Import form csv")
        message = 'Thanks a lot, the points were added to the map!'
        message_type = 'success'
    else:
        message = error_message
        message_type = 'alert'

    return render(
        request, 'www/basic_message.html', {
            'message': message,
            'message_type': message_type,
        })


def get_and_check_csv_data(
        required_fields, optional_fields, header_line, other_lines):
    """
    Return a list of dictionaries that represent each point to add. The key of
    the dictionary is the name of the parameter of the object Marker.

    :param required_fields: The name of the required fields
    :param optional_fields: The name of the optional fields
    :param header_line: The header lines get from csv.reader call
    :param other_lines: The other lines get from csv.reader call
    :returns: A tuple of 2 elements : the first element is the array list of
        dictionaries (if everithing went well), the second element is the
        error message.
    """
    mapping = {}
    header_uppercase = [h.upper() for h in header_line]

    # Check if the require fields are in the header
    for fn in required_fields:
        fn_upper = fn.upper()
        if fn_upper not in header_uppercase:
            return (None, '{} not in the header.'.format(fn))
        else:
            mapping[fn] = header_uppercase.index(fn_upper)

    # Add the optional fields
    for fn in optional_fields:
        fn_upper = fn.upper()
        if fn_upper in header_uppercase:
            mapping[fn] = header_uppercase.index(fn_upper)

    # Read the liens for each points
    points = []
    for line in other_lines:
        # Create a dictionary with the received data
        point = {}
        for field_name, pos in mapping.items():
            # Traiter ligne vide
            point[field_name] = line[pos]

        # Check if we have an empty line
        empty_line = True
        for fn in required_fields:
            if point[fn] != '':
                empty_line = False
                break

        if not empty_line:
            # Check if the the required_fields are completed
            for fn in required_fields:
                if point[fn] == '':
                    return (
                        None, 'One line has its field "{}" empty'.format(fn))

            point['lat'] = float(point['lat'])
            point['lon'] = float(point['lon'])

            # Check if lat / long is good
            if point['lat'] > 90 or point['lat'] < -90 or \
                    point['lon'] > 180 or point['lon'] < -180:
                return (
                    None, 'It is not possible to create the point {} \
with lat: {} and lon: {}'
                    .format(point['name'], point['lat'], point['lon']))
            points.append(point)

    return (points, None)
