# DeweyMaps

## Installation:
* Install postgresql and postgis
* virtualenv  (in python 3 -   virtualenv --python=/usr/bin/python3 bmde-env)
* pip install -r requirements.txt
* createdb maps (as postgres user)
* ./manage.py migrate


## Installation serveur

1. Mise à jour dépendances postgres

Voir https://www.postgresql.org/download/linux/ubuntu/

2. Installation postgresql & postgis

```
$ sudo apt-get install postgresql-server-dev-9.5 postgresql-9.5-postgis-2.2
```

2.bis Installation dep pour pyscopg2

```
$ sudo apt-get install python3-dev gcc libpq-dev
```

3. Installation de pip3 & virtualenv

```
$ wget https://bootstrap.pypa.io/get-pip.py #see https://pip.pypa.io/en/stable/installing/
$ python3 get-pip.py
$ pip install virtualenv
```

4. Création de l'env virtuel

```
$ virtualenv --python=/usr/bin/python3 ve
```


5. Création de la db maps

```
$ su - u postgres
postgres$ createuser bmde -W
postgres$ createdb -O bmde -E utf8 bmde
postgres$ psql -c "Create exetension postgis" -d bmde
```

6. Installation des dépendances:

```
$ source ve/bin/activate
$ pip install -r requirements.txt
```

7. Mettre à jour la config:

```
$ cp settings.py.dist settings.py
$ vi settings.py #editer db
$ ./manage.py migrate
```

8. Configurer apache

```
$ apt-get install apache2 libapache2-mod-wsgi-py3
```

et éditer le fichier de conf:

```
<VirtualHost *:80>
	Alias /static /var/www/bmde/static
	<Directory /var/www/bmde/static>
		Require all granted
	</Directory>

	<Directory /var/www/bmde/www>
		<Files wsgi.py>
			Require all granted
		</Files>
	</Directory>

	WSGIDaemonProcess bmde python-home=/var/www/bmde/ve python-path=/var/www/bmde
	WSGIProcessGroup bmde
	WSGIScriptAlias / /var/www/bmde/www/wsgi.py
</VirtualHost>
```
